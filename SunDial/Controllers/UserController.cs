﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace SunDial.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private static string[] Names =
        {
            "Moses", "Diego", "Dan Bottiglieri", "Rumes", "Marc", "Razvan", "Krupa"
        };

        private static string[] Roles =
        {
            "Front End", "Back End", "Project Manager", "Tester", "Full-Stack Developer"
        };

        [HttpGet("[action]")]
        public IEnumerable<UserAccount> Users()
        {
            var rng = new Random();
            var users = Enumerable.Range(1, 5).Select(index => new UserAccount
            {
                Name = Names[rng.Next(Names.Length)],
                Role = Roles[rng.Next(Roles.Length)]
            });
            while (users.Select(n => Names).Distinct().Count() == 5)
            {
                users = Enumerable.Range(1, 5).Select(index => new UserAccount
                {
                    Name = Names[rng.Next(Names.Length)],
                    Role = Roles[rng.Next(Roles.Length)]
                });
            }
            return users;
        }

        public class UserAccount
        {
            public string Name;
            public string Role;
        }
    }
}
