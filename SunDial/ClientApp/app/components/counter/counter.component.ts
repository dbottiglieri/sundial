import { Component } from '@angular/core';

@Component({
    selector: 'counter',
    templateUrl: './counter.component.html'
})
export class CounterComponent {
  public currentCount = 0;
  public secretCode: string;

    public incrementCounter() {
        this.currentCount ++;
  }

  public decrementCounter() {
    this.currentCount--;
  }

    public getSquareRoot() {
        this.currentCount = Math.sqrt(this.currentCount);
  }

  public getFactorial(seed: number) {
    if (seed > 1) {
      this.currentCount *= --seed;
      this.getFactorial(seed);
    }
  }

  public factorialOfCounter() {
    if (this.currentCount > 0)
      this.getFactorial(this.currentCount);
    else {
      this.getFactorial(this.currentCount * -1);
    }
  }

  public resetToZero() {
    this.currentCount = 0;
  }

  public cipher() {
    let output: string;
    output = "";
    for (let letter of this.secretCode) {
      if (/[A-Za-z]/.test(letter))
        output += String.fromCharCode((letter.charCodeAt(0) + (this.currentCount % ('a'.charCodeAt(0) + 26))));
      else
        output += letter;
    }
    this.secretCode = output;
  }

  public onKey(event: KeyboardEvent) {
    this.secretCode = (<HTMLInputElement>event.target).value;
  }
}
